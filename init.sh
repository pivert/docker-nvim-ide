#!/bin/bash
# You can export for instance DEBUG=2 when starting the container

set -e

ARGV=($@)
ARGC=$#
DEBUG=${DEBUG:-0}


if (( $DEBUG > 1 ))
then
    set -x
fi

log () {
    # Only print info help when executed as login shell
    if (( $DEBUG > 0 ))
    then
        echo "$@"
    fi
}

print_help () {
    sed -n '/# Start the container/,$ p' /README.md
}

log "Starting /init.sh with ARGV=$ARGV and DEBUG=$DEBUG"
if [[ ! -d /workdir ]]
then
    print_help

    log """

If you're using ssh-agent on the host, add:
    -v $(readlink -f $SSH_AUTH_SOCK):/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent

You can mount any file for your setup, such as (if exists)
    -v /etc/ansible/hosts:/etc/ansible/hosts 
"""
fi

cd /workdir/

if [[ -f /etc/passwdhost && -f /etc/grouphost ]]
then
    log "Copying UIDs and GIDs >= 500 to local /etc/passwd and /etc/group from /etc/passwdhost and /etc/grouphost"
    awk -F: '($3 >= 500 ) {print $0}' /etc/passwdhost >> /etc/passwd
    awk -F: '($3 >= 500 ) {print $0}' /etc/grouphost >> /etc/group

else
    log "OPTIONAL: Cannot map UID & GID from host"
    log "    You should mount your /etc/passwd to /etc/passwdhost (RO) for UID translation"
    log "    and mount your /etc/group to /etc/grouphost (RO) for UID translation."
    WARN=true
fi

ROOTINIT=/workdir/docker-environment-root.sh
if [[ -f ${ROOTINIT} ]]
then
    if  [[ -x ${ROOTINIT} ]]
    then
	log "Executing: ${ROOTINIT}"
	${ROOTINIT}
    else
	log "${ROOTINIT} exists, but is not executable"
    fi
else
    log "You can provide an executable in the root folder run as root : ${ROOTINIT}"
fi

log "Exporting the environment variables that needs to be preserved and parsing IDTO environment variable"

read UIDTO USERTO GIDTO GNAMETO <<< $(echo $IDTO | sed -Ee 's/uid=([[:digit:]]+)\(([[:alnum:]@.]+)\) gid=([[:digit:]]+)\(([[:alnum:]@.]+)\) .*/\1 \2 \3 \4/')
if [[ $?==0 ]] && [[ ! -z ${UIDTO} ]] && [[ ! -z ${GIDTO} ]]
then
    # Rename the current tab in Konsole or equivalent terminal emulator
    chown ${UIDTO}:${GIDTO} ${HOME}

    if [[ ${UIDTO}!=0 ]]
    # Non root UID
    then
    # Ensure the user group exists in /etc/group, create it if needed.
        if [[ $( grep ":${GIDTO}:" /etc/group | wc -l ) == 1  ]]
        # GID exists in /etc/group
        then
            GROUPNAME=$( grep ":${GIDTO}:" /etc/group | cut -d ':' -f 1 )
        else
        # Group not in /etc/group
            log "Group GID=${GIDTO} was not found in /etc/group (from /etc/grouphost)"
            if [ -z ${GNAMETO+x} ]
            then
                log 'Group name not provided by the $IDTO variable. Default to "usergroup" group.'
                GROUPNAME="usergroup"
            else
                GROUPNAME=${GNAMETO}
                log "Executing: groupadd -g ${GIDTO} ${GROUPNAME}"
                groupadd -g ${GIDTO} ${GROUPNAME} || :   # Allow fail
            fi
        fi
    else
        # root UID
        GROUPNAME='root'
    fi

    # Ensure the user exists in /etc/passwd
    if id "${UIDTO}" &>/dev/null; then
        USERNAME=$(id -nu ${UIDTO})
        log "User wih UID=${UIDTO} found: USERNAME=${USERNAME}"
    else
        log "User for UID=${UIDTO} was not found in /etc/passwd (from /etc/passwdhost)"
        if [ -z ${USERTO+x} ]
        then
            log 'The $IDTO does not contain the username. Default to user "user"'
            USERNAME="user"
        else
            USERNAME=${USERTO}
        fi
        log "Execuring: useradd -u ${UIDTO} -g ${GIDTO} -s /bin/bash -M -d /user ${USERNAME}"
        useradd -u ${UIDTO} -g ${GIDTO} -s /bin/bash -M -d /user ${USERNAME}
    fi

    log "enable 'su -' if needed (clear root password)"
    echo "root:" | chpasswd -e
    echo "${USERNAME}:" | chpasswd -e
    usermod -d /user "${USERNAME}" # Change the home to be /user



    if (( ${#ARGV} > 0 ))
    then
      CMD="${ARGV[@]}"
      log "Changing to user ${USERNAME} with UID:${UIDTO} and GID:${GIDTO} and execute ${CMD} in a login shell"
      su -p ${USERNAME} -c "${CMD}"
    else
	log "Changing to user ${USERNAME} with UID:${UIDTO} and GID:${GIDTO} and execute login shell"
    	su -l -w RSA_PUBKEY,SSH_AUTH_SOCK ${USERNAME}
    fi
else
    log "User detection failed: [[ $?==0 ]] && [[ ! -z ${UIDTO} ]] && [[ ! -z ${GIDTO} ]]"
    # Rename the current tab in Konsole or equivalent terminal emulator
    echo "WARNING - Starting bash as ROOT (Check the /README.md)"
    exec "$@"
fi
