# Python - Neovim Development Environment

Welcome to packaged Neovim IDE with latest stable neovim, coc-pyright, python, ansible, npm, and nodejs.

This image is intended to be used as a workstation for developing in python, js or just editing files.
It includes many packages, clients and tools, including :
kubectl, aws, sam (aws-sam for Serverless Application Model), docker, ssh, git, xz, unzip, nmap, nc, rsync, vue (vuejs), linters for yaml, js, Ansible, Dockerfile, ...


# How to start
When started started with the nvimdocker script, it will try to detect your user id, and su to it so you keep the same access rights and are not root by default (but you can su - in the image if needed):
- Make sure you have docker executable installed and that your user has access to it. (i.e. you're member of the docker group)
- cd to a folder in your $PATH, for instance in /usr/local/bin as root, or in your $HOME/bin
- Download the runner script:

    curl -o nvimdocker https://gitlab.com/pivert/ansible-neovim-pyright/-/raw/main/nvimdocker && chmod a+x nvimdocker

- Run it:

    ./nvimdocker                # To get a shell with tmux, neovim, python, pyright, ansible, ...
          OR
    ./nvimdocker vim file.py    # To start editing directly in the environment. Template will be used if file.py does not exists.



# Colors & Fonts
- Terminal should use a Solarized Dark color scheme.
- Use Powerline patched fonts such as [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts)

## Command details

# Features:
- Syntax for Python, Ansible, Dockerfile, vimrc, YAML & JSON
- Installed software: git tmux neovim(latest) nodejs(latest) npm curl wget htop...
- Latest Ansible is installed
- Default neovim from bullseyes slim image with plugins:
  - Python, Dockerfile & Bash basic templates for new scripts
  - neoclide/coc.nvim intellisense engine with several plugins:
    - [coc-pyright](https://github.com/fannheyward/coc-pyright)
    - [coc-spell-checker](https://github.com/iamcco/coc-spell-checker)
    - [coc-yaml](https://github.com/neoclide/coc-yaml)
    - [coc-json](https://github.com/neoclide/coc-json)
    - [coc-git](https://github.com/neoclide/coc-git)
    - [coc-prettier](https://github.com/neoclide/coc-prettier)
    - [coc-docker](https://github.com/josa42/coc-docker)
    - [coc-ansible](https://github.com/yaegassy/coc-ansible)
  - kien/ctrlp.vim           [Fuzzy finder for vim](https://github.com/kien/ctrlp.vim)
  - scrooloose/nerdcommenter [Comment/Uncomment tool](https://github.com/preservim/nerdcommenter#usage)
  - scrooloose/nerdtree               A Tree-like side bar for better navigation
  - tmhedberg/matchit                 Switch to the begining and the end of a block by pressing %
  - vim-airline/vim-airline           A cool status bar
  - vim-airline/vim-airline-themes    Airline themes
  - altercation/vim-colors-solarized  Solarized colorscheme (default to dark theme)
  - arcticicestudio/nord-vim          Nord
  - sheerun/vim-polyglot              Better syntax-highlighting for filetypes in vim
  - tpope/vim-fugitive                Git integration
  - tpope/vim-surround                Surround.vim is all about "surroundings": parentheses, brackets, quotes, XML tags, and more.
  - jiangmiao/auto-pairs              Auto-close braces and scopes
  - jpalardy/vim-slime', { 'for': 'python' } 
  - hanschen/vim-ipython-cell', { 'for': 'python' } Tmux integration
  - tmhedberg/SimpylFold              Python code folding - zo zO zc zC
- Solarized terminal colors for:
  - tmux
  - bash
  - neovim
- Other tools
  - AWS Serverless Application Model tools: aws-cli and sam
  - vue
  - Most command-line tools such as git, rsync, nc, nmap, unzip, xz,...

## Neovim
- [Neovim Documentation](https://neovim.io/doc/general/)
- use :checkhealth to get status of the editor setup
- ]g and [g go to next/previous coc diagnostic error/warning
- Basic buffer commands (read help for more):
  - :bn[ext]/:bp[revious]/:bf[irst]/:bl[ast]/:b2/:bc[lose] : go to buffer next/previous/first/last/second/close current buffer
  - :e[dit] newfile.py : Create a new buffer, add it to the list, and open the newfile.py in it
- Check installed Plug plugins : PlugStatus
- Check installed Coc extensions : CocList extensions

## Quick tips
- tmux: C-b instead of screen C-b a / C-b-% C-b-" C-o C-b-,
- vim : :checkhealth :CocList :CocInfo
- vim-slime + tmux + ipython : C-c C-c from nvim
- vim-surround: cs'" / S" (visual) / ysiw(
- vim Leader key is Space key: Space-r-n (rename symbol)   
- Try :CocAction :CocCommands :verbose
- vim-fold: za/zA zo/zO zc/zC zm/zr
                                                           

## SSH
You might want to use ssh with pubkey authentication from the container.
You might also want to cache your ssh key password.
The first thing is to mount your .ssh folder when running the container.
Add -v ~/.ssh:/user/.ssh 

Then you have 2 options:
- run 'eval $(ssh-agent) > /dev/null' then 'ssh-add' in the container.
- run ssh-add before running the container, then reuse the agent by adding 
  '-f $SSH_AUTH_SOCK):/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent' to your docker run command

## Customization
Most customization can be done by mounting updated config file and/or 
adapting the nvimdocker local shell script.

You can place an executable init script in the root of the folder named
docker-environment-root.sh (executed as root before the su -l)

## Further Help
Check online documentation from tmux, nvim, and the plugin you would like to use.
Also check SLIME and ipython-cell plugin for Tmux & ipython integrations.
An alias has been added to read the relevant config files, README.md and environment:

Just run 'config' command from bash in the container.

Source code & issues: https://gitlab.com/pivert/ansible-neovim-pyright/ 

## Security
Since most of the time the docker daemon runs as root, this image requires starting as root, and provides any user root access to the host.
