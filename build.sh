#!/bin/bash
VERSION='1.40'
docker build --no-cache -t "pivert/ansible-neovim-pyright:$VERSION" -t 'pivert/ansible-neovim-pyright:latest' .
