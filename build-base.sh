#!/bin/bash
VERSION='1.39b3'
docker build --no-cache -t "pivert/python-neovim:$VERSION" -t 'pivert/python-neovim:latest' -f Dockerfile-base .
