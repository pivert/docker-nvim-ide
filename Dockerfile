FROM python:3.10-slim

LABEL maintainer="François Delpierre <docker@pivert.org>"

ENV TZ=Etc/UTC

RUN apt-get update && apt-get install -y \
  auto-apt-proxy \
  curl \
  git \
  make \
  gnupg \
  ssh-client \
  wget \
  xz-utils \
  yamllint \
  unzip \
  && apt-get clean

WORKDIR /build

# Install node repository
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -y nodejs && apt-get clean

# Node dependencies
RUN npm install -g --no-update-notifier npm \
  && npm install -g dockerfile-language-server-nodejs neovim \
  && npm cache clean --force \
  && rm -rf /user/.cache

COPY build/ /build/

ARG name=defaultValue

# Build nvim from source
RUN ./buildnvim.sh && ./sam-cli-install.sh

# Install command line tools
RUN apt-get update && apt-get install -y --no-install-recommends \
  tmux/stable \
  htop/stable \
  netcat-openbsd/stable \
  bash-completion \
  command-not-found \
  xclip \
  iproute2 \
  iputils-ping \
  rsync \
  man \
  less \
  procps \
  dialog \
  nmap \
  iputils-ping \
  python3-venv \
  awscli \
  kubernetes-client \
  docker.io \
  && apt-file update \
  && update-command-not-found \
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* To be added when image is ready

COPY init.sh README.md untaransiblecollection.sh user /
COPY user/ /user/
COPY etc/ /etc/

ENV HOME=/user
ENV PATH=/usr/bin:/user/.local/bin:$PATH

RUN python -m pip install --no-cache-dir --upgrade pip 
RUN pip install --no-cache-dir -r requirements.txt 

# Uncomment the 2 below lines to reduce from 500MB to 50MB the Ansile Collection.
#   && tar -Jcf /usr/local/lib/python3.10/site-packages/ansible_collections.tar.xz /usr/local/lib/python3.10/site-packages/ansible_collections \
#   && rm -rf /usr/local/lib/python3.10/site-packages/ansible_collections
 
# Neovim Plug Install & Plugins install
RUN sh -c 'curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# Take only the Plug part since Coc part will fail :
RUN sed '/call plug#end()/q' /user/.config/nvim/init.vim > /tmp/vimpluginit.vim
RUN nvim -e -u /tmp/vimpluginit.vim -i NONE -c "PlugInstall|q" -c "qa" \
  || echo "Some Neovim Plug plugins failed to install."

# Install node packages for Coc to work - Ignore errors if a plugin is not fully installed
# Info: The vue & dependancies adds 100MB to the compressed image.
RUN nvim -e -c "CocInstall -sync coc-spell-checker coc-git coc-pyright coc-json coc-docker coc-yaml @yaegassy/coc-ansible coc-prettier @yaegassy/coc-volar|q" || echo "Some Neovim Coc plugins failed to install"
RUN npm install -f @vue/cli \
  && npm install eslint eslint-plugin-vue -D \
  && npm cache clean --force \
  && rm -rf /user/.cache
#  && rm -rf /user/.npm \
RUN chmod -R a+rwX /user

WORKDIR /workdir
RUN chmod a+x /init.sh
ENTRYPOINT [ "/init.sh" ]
CMD [ "/bin/bash", "-l" ]
