#!/bin/bash
#
# Since it might not be used all the time, the default Ansible Collection is
# compressed (30MB instead of 500MB)
#
# This command untars it:
tar -Jxvf /usr/local/lib/python3.10/site-packages/ansible_collections.tar.xz -C /
