#!/bin/bash
apt-get install -y ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config doxygen
git clone --depth 1 --branch "v0.7.0" https://github.com/neovim/neovim.git
cd neovim
make CMAKE_BUILD_TYPE=Release
make install
cd ..

# Clean
rm -rf neovim
apt-get purge -y ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config doxygen
apt-get autoremove -y
apt-get clean

# Set shortcuts
NVIM_PATH=/usr/local/bin
cp -a bin/* $NVIM_PATH
NVIM_PATH_EXE="${NVIM_PATH}/nvim"
update-alternatives --install /usr/bin/vi vi "$NVIM_PATH_EXE" 110
update-alternatives --set vi "$NVIM_PATH_EXE"

update-alternatives --install /usr/bin/vim vim "$NVIM_PATH_EXE" 110
update-alternatives --set vim "/usr/local/nvim"

update-alternatives --install /usr/bin/editor neovim "$NVIM_PATH_EXE" 1

update-alternatives --install /usr/bin/view view "${NVIM_PATH}/view" 110
update-alternatives --set view "${NVIM_PATH}/view"

update-alternatives --install /usr/bin/ex ex "${NVIM_PATH}/ex" 110
update-alternatives --set ex "${NVIM_PATH}/ex"

update-alternatives --install /usr/bin/vimdiff vimdiff "${NVIM_PATH}/vimdiff" 110
update-alternatives --set vimdiff "${NVIM_PATH}/vimdiff"
